
import * as Scene from "./app.js";

var ws;
var reader = new FileReader();


window.addEventListener("load", function(evt) {
    
    connectWS();

    function connectWS(evt) {
        console.log("OPEN MG");
        if (ws) {
            return false;
        }

        //ws = io.connect('http://localhost:5000');
        ws = io();
        ws.on('after connect', function(msg) {
            console.log('After connect', msg);
            //Scene.initDrone(176);
        });

        ws.on("init_drone",function(msg){
            console.log("connection from drone");
            //console.log(msg)
            Scene.initDrone(msg[0],msg[1]);
        });

        ws.on('to_viz', function(msg) {
            // console.log(msg)
            Scene.updateDrone(msg)
            //Scene.initDrone(176);
        });

        ws.on('disconnect', function(){
            console.log('sd')
            
            // setTimeout(function(){
            //     let ret = connectWS();
            //     if(ret.connected){
            //         console.log("connected");
            //     }
            // }, 3000);
            
        });
        
        return false;
        };
    
});
//var i = 0
function sendMessage(drone){
    drone.data.counter=0;
    // console.log(Date.now()+":  "+drone.data.multi.back);
    ws.emit("from_viz",JSON.stringify(drone.data));
    // console.log("sM")
    // console.log(drone);
}

function _getTime(){
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var hour = d.getHours();

    var min = d.getMinutes();

    var secs = d.getSeconds();
    var mil = d.getMilliseconds();
    var str = "["+year+"-"+month+"-"+day+" "+hour+":"+min+":"+secs+","+mil+"]";
    return str;
}

export {sendMessage}