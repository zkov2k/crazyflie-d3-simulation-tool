import logging
import sys

def configureLogger(name=None, level=logging.DEBUG):
	# Log file location
	if name == None:
		name = "logfile"
		
	logfile = 'logs/'+name+'.log'
	# if name!=None:
	# 	logfile = 'logs/'+name+'.log'
	# Define the log format
	log_format = ('[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s')

	filehandler = logging.FileHandler(logfile)
	filehandler.setLevel(logging.DEBUG)
	filehandler.setFormatter(logging.Formatter(log_format))

	streamhandler = logging.StreamHandler(sys.stdout)
	streamhandler.setLevel(logging.INFO)
	streamhandler.setFormatter(logging.Formatter(log_format))

	log = logging.getLogger(name)
	log.setLevel(logging.DEBUG)

	log.addHandler(filehandler)
	log.addHandler(streamhandler)
	#log.addHandler(streamhandler)
	#log.setFormatter(log_format)
	#logging.basicConfig(level=logging.DEBUG,format=log_format)#, filename=logfile)
	#logging.basicConfig(format=log_format)

	# # Define basic configuration
	# logging.basicConfig(
	#     # Define logging level
	#     level=level,
	#     # Declare the object we created to format the log messages
	#     format=log_format,
	#     # Declare handlers
	#     handlers=[
	#         logging.FileHandler(logfile),
	#         logging.StreamHandler(sys.stdout),
	#     ]
	# )
	
	return log