#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2011-2013 Bitcraze AB
#
#  Crazyflie Nano Quadcopter Client
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.
"""
Crazyflie USB driver.

This driver is used to communicate with the Crazyflie using the USB connection.
"""
import logging
import queue
import re

import socketio
import json
import struct
import traceback 
import signal
import sys
import time

from cflib.crtp.crtpstack import CRTPPacket
from cflib.crtp.exceptions import WrongUriType
from cflib.crtp.crtpdriver import CRTPDriver


import src.logHandler as logHandler

log = logHandler.configureLogger("wsDrirver", logging.WARNING)



#sio = socketio.Client(logger=True, engineio_logger=True)


class WebSocketDriver(CRTPDriver):
    """ Crazyradio link driver """

    def __init__(self):
        """ Create the link driver """
        CRTPDriver.__init__(self)
        #self.cfusb = None
        self._is_conncted = False
        self.uri = ''
        self.link_error_callback = None
        self.link_quality_callback = None
        self.in_queue = None
        self.out_queue = None
        self._thread = None
        self.needs_resending = False
        self.sio = socketio.Client()
        # self.sio = socketio.Client(logger=True, engineio_logger=True)
        self.sio.on("connect",self.connected)
        self.sio.on("disconnect",self.disconnect)
        self.sio.on("to_drone",self.to_drone)
        self.sio.on("drone_response",self.drone_response)
        #signal.signal(signal.SIGINT, self.signal_handler) # ctrl+c



    #def signal_handler(signum, **_):
    def signal_handler(self,signum, x):
        log.info(x)
        message = f'Signal handler called with signal {signum}'
        log.info(message)
        # do things
        #self.sio.disconnect()
        #self.close()
        #sys.exit(0)
        
        

    def connect(self, uri, link_quality_callback, link_error_callback):
        """
        Connect the link driver to a specified URI of the format:
        radio://<dongle nbr>/<radio channel>/[250K,1M,2M]

        The callback for linkQuality can be called at any moment from the
        driver to report back the link quality in percentage. The
        callback from linkError will be called when a error occurs with
        an error message.
        """

        # check if the URI is a radio URI
        if not re.search('^http://', uri):
            raise WrongUriType('Not a WebSocket URI')

        # Open the USB dongle
        # if not re.search('^usb://([0-9]+)$',
        #                  uri):
        #     raise WrongUriType('Wrong radio URI format!')

        # uri_data = re.search('^usb://([0-9]+)$',
        #                      uri)

        self.uri = uri
        
        # if self.cfusb is None:
        #     self.cfusb = CfUsb(devid=int(uri_data.group(1)))
        #     if self.cfusb.dev:
        #         self.cfusb.set_crtp_to_usb(True)
        #     else:
        #         self.cfusb = None
        #         raise Exception('Could not open {}'.format(self.uri))

        # else:
        #     raise Exception('Link already open!')

        # Prepare the inter-thread communication queue
        #self.in_queue = queue.Queue()
        
        self.in_queue = queue.Queue()
        
        # Limited size out queue to avoid "ReadBack" effect
        self.out_queue = queue.Queue(50)
        self.sio.connect(self.uri)
        while not self._is_conncted:
            time.sleep(0.01)
        # Launch the comm thread
        # self._thread = _UsbReceiveThread(self.cfusb, self.in_queue,
        #                                  link_quality_callback,
        #                                  link_error_callback)
        # self._thread.start()

        self.link_error_callback = link_error_callback

    def receive_packet(self, time=0):
        """
        Receive a packet though the link. This call is blocking but will
        timeout and return None if a timeout is supplied.
        """
        packet = None
        if time == 0:
            try:
                #return self.in_queue.get(False)
                packet = self.in_queue.get(False)
            except queue.Empty:
                return None
        elif time < 0:
            try:
                #return self.in_queue.get(True)
                packet = self.in_queue.get(True)
            except queue.Empty:
                return None
        else:
            try:
                #return self.in_queue.get(True, time)
                packet =  self.in_queue.get(True, time)
            except queue.Empty:
                return None
        # print(packet)
        return packet

    def _unpackStruct(self, pk):
        types = ['<BBBfffff','<BBff?f','<Bffff']
        data = {}
        for t in types:
            try:
                tmp = struct.unpack(t,pk)
                if t == '<BBBfffff':
                    data["position"]={}
                    data["position"]["x"]= tmp[3]
                    data["position"]["y"]= tmp[4]
                    data["position"]["z"]= tmp[5]
                    data = json.dumps(data)
                elif t== '<BBff?f':
                    data["position"]= {}
                    data["position"]["z"]=tmp[2]
                    data = json.dumps(data)
                elif t=='<Bffff':
                    data["velocity"] = {}
                    data["velocity"]["vx"] = tmp[1]
                    data["velocity"]["vy"] = tmp[2]
                    data["velocity"]["yawrate"] = tmp[3]
                    data["velocity"]["zdistance"] = tmp[4]
                    data = json.dumps(data)
            except:
                pass
                #print("not able to unpack with "+str(t))
            
        return data

    def send_packet(self, pk):
        """ Send the packet pk though the link """
        # if self.out_queue.full():
        #    self.out_queue.get()
        # if (self.cfusb is None):
        #     return
        if not self._is_conncted:
            #log.info("Trying to send data on closed socket")
            return
        try:
            data = pk
            if isinstance(pk,CRTPPacket):
                data = self._unpackStruct(pk.data)
                if len(data)==0:
                    return
                #data = json.dumps(struct.unpack('<Bffff',pk.data))
                # tmp = struct.unpack('<BBBfffff',pk.data)
                # data = {}
                # data["position"]={}
                # data["position"]["x"]= tmp[3]
                # data["position"]["y"]= tmp[4]
                # data["position"]["z"]= tmp[5]
                # data = json.dumps(data)
                # self._send_packet(struct.pack('<BBBfffff',
                #                       self.COMMAND_GO_TO,
                #                       group_mask,
                #                       relative,
                #                       x, y, z,
                #                       yaw,
                #                       duration_s))
                #(4, 0, 0, 200.0, 200.0, 50.0, 0.0, 957.1383666992188)

            #log.warning("send: "+str(data))
            self.sio.emit("from_drone", data)
        except:
            #pass
            log.info("not able to send data")
            log.info(pk)
            traceback.print_exc() 
        #print("Its sent")
        # try:
        # dataOut = (pk.header,)
        # dataOut += pk.datat
        #self.cfusb.send_packet(dataOut)
        #print(dataOut)
        
        # except queue.Full:
        #     if self.link_error_callback:
        #         self.link_error_callback(
        #             'WebSocketDriver: Could not send packet to Crazyflie')

    def pause(self):
        pass
        #self._thread.stop()
        #self._thread = None


    def restart(self):
        #if self._thread:
         #   return
         sio.connect(self.uri)
        #self._thread = _WSReceiveThread(sio, self.in_queue, self.link_quality_callback, self.link_error_callback)
        #self._thread.start()

    def close(self):
        """ Close the link. """
        # Stop the comm thread
        #self._thread.stop()

        # Close the USB dongle
        try:
            if self._is_conncted:
                self.sio.disconnect()
                #self.cfusb.set_crtp_to_usb(False)
                #self.cfusb.close()
        except Exception as e:
            # If we pull out the dongle we will not make this call
            logger.info('Could not close {}'.format(e))
            pass
        self._is_conncted=False

    def scan_interface(self, address):
        if self._is_conncted:
            return [self.uri]
        return []

    def get_status(self):
        return 'No information available'

    def get_name(self):
        return 'WSCdc'

    def initDrone(self, id, position):
        log.info("init new drone: "+str(id)+"  "+str(position))
        data = [id, position]
        self.sio.emit("new_drone",data)
        
    def connected(self):
        self._is_conncted = True
        #print('connected to WS server')
        log.info('connected to WS server')


        

    
    def disconnect(self):
        self._is_conncted = False
        try:
            # print('disconnected from WS server')
            log.info('disconnected from WS server')
        except:
            pass


    def drone_response(self,drone):
        pass
        #print('Client disconnected!!')
        #print(sid)
        #NOTE: convert distance to meters
        # print(drone)
        # print("FROM DRONE")
        # pk = CRTPPacket(243, drone)
        # self.in_queue.put(pk)
        # pk = CRTPPacket(247, drone)
        # self.in_queue.put(pk)
        # pk = CRTPPacket(209, drone)
        # self.in_queue.put(pk)

    def to_drone(self,drone):
        # if drone["range.back"]>1200:
        #     log.info(drone["range.back"])
        # print("TO DRONE")
        #log.info(drone["range.front"])
        # if drone["counter"]%10==0:
        #     log.info(drone["counter"])
        #with q.mutex:
        #self.in_queue.queue.clear()
        self.in_queue.put(drone)
        




# # Transmit/receive radio thread
# class _WSReceiveThread(threading.Thread):
#     """
#     Radio link receiver thread used to read data from the
#     Crazyradio USB driver. """

#     def __init__(self, sio, inQueue, link_quality_callback,link_error_callback):
#         """ Create the object """
#         threading.Thread.__init__(self)
#         self.sio = sio
#         self.in_queue = inQueue
#         self.sp = False
#         self.link_error_callback = link_error_callback
#         self.link_quality_callback = link_quality_callback

#     def stop(self):
#         """ Stop the thread """
#         self.sp = True
#         try:
#             self.join()
#         except Exception:
#             pass

#     def run(self):
#         """ Run the receiver thread """

#         while (True):
#             if (self.sp):
#                 break
#             try:
#                 # Blocking until USB data available
#                 data = self.cfusb.receive_packet()
#                 if len(data) > 0:
#                     pk = CRTPPacket(data[0], list(data[1:]))
#                     self.in_queue.put(pk)
#             except Exception as e:
#                 import traceback

#                 self.link_error_callback(
#                     'Error communicating with the Crazyflie'
#                     ' ,it has probably been unplugged!\n'
#                     'Exception:%s\n\n%s' % (e,
#                                             traceback.format_exc()))
