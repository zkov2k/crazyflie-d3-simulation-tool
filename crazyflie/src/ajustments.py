import logging

import src.logHandler as logHandler

#log = logging.getLogger("testing")
log = logHandler.configureLogger("ajustments", logging.WARNING)



MIN_DISTANCE = 0.3
CATASTROPHE_DISTANCE=0.1

def checkSensors(sensors):
	#check sensors to see if something is too close
	currentSensors = {}
	currentSensors["up"] = sensors.up
	currentSensors["down"] = sensors.down
	currentSensors["front"] = sensors.front
	currentSensors["back"] = sensors.back
	currentSensors["left"] = sensors.left
	currentSensors["right"] = sensors.right

	#test for start condidtions
	sum_ = 0
    
	for i in currentSensors.values():
		if i is not None:
			sum_ +=i
	if sum_==0:
		#start condition
		return None

	reajustments = []
	for direction,distance in currentSensors.items():
		log.debug(str(direction)+"  "+str(distance))
		if direction != "down" and distance<MIN_DISTANCE:
			reajustments.append(direction)
		#log.info(direction+". "+str(distance))
		if distance<=CATASTROPHE_DISTANCE and direction != "down":
			return ("CATASTROPHE", currentSensors)
	log.debug("-----------------")
	if len(reajustments)>0:
		return ("REAJUSTMENTS",reajustments)
	else:
		return None
            