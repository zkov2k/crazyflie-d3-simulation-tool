import logging
import math
import threading
import time

from src.motionController import MotionController

import logging
import src.logHandler as logHandler

log = logHandler.configureLogger("sensorthread", logging.WARNING)

MIN_DISTANCE = 0.3
CATASTROPHE_DISTANCE=0.05

# Transmit/receive radio thread
class SensorThread(threading.Thread):
	"""
	Radio link receiver thread used to read data from the
	Crazyradio USB driver. """

	def __init__(self, callback, local,multiranger, motionController):
		""" Create the object """
		threading.Thread.__init__(self)
		self.daemon=True
		log.info("Init SensorThread")
		self._mc = motionController
		self._sp = False
		self._callback = callback
		self._local = local
		self._multiranger = multiranger
		self._detouring = False
		self._averageHeight = 0

	def stop(self):
		log.info("stopped sensor thread")
		self._sp = True
		try:
			self.join()
		except Exception:
			pass



	def run(self):
		""" Run the receiver thread """
		log.info("sensorthread started")
		while not self._sp:
			# log.debug(self._multiranger.down)
			log.debug(str(self._local.position))
			detour = self._checkSensors(self._multiranger)

			#log.warning(detour)
			if detour!=None:
				#self._detouring=True
				self._callback(True,detour, self._local,self._multiranger)

			else:
				self._callback(False,None,None)
				
			time.sleep(0.0075)




	def _checkSensors(self, sensors):

		#check sensors to see if something is too close
		currentSensors = {}
		currentSensors["up"] = sensors.up
		currentSensors["down"] = sensors.down
		currentSensors["front"] = sensors.front
		currentSensors["back"] = sensors.back
		currentSensors["left"] = sensors.left
		currentSensors["right"] = sensors.right

		#test for start condidtions
		sum_ = 0
        
		for i in currentSensors.values():
			if i is not None:
				sum_ +=i
		if sum_==0:
			#start condition
			return None

		reajustments = []
		for direction,distance in currentSensors.items():
			if distance is not None:
				if  distance<MIN_DISTANCE:
					reajustments.append(direction)
				#log.info(direction+". "+str(distance))
				if distance<=CATASTROPHE_DISTANCE and direction != "down":
					return ("CATASTROPHE", currentSensors)	


		if len(reajustments)>0:
			return ("REAJUSTMENTS",reajustments)
		else:
			return None
            
