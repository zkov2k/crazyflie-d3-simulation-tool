import * as THREE from './three/build/three.module.js';


var deltaMovement = 0.5 //cm
var downVector = new THREE.Vector3( 0, 0, -1 );
var upVector = new THREE.Vector3( 0, 0, 1 );
var frontVector = new THREE.Vector3( 1, 0, 0 );
var backVector = new THREE.Vector3( -1, 0, 0 );
var rightVector = new THREE.Vector3( 0, 1, 0 );
var leftVector = new THREE.Vector3( 0, -1, 0 );

var lastTime = Date.now()
var deltaTime =25 //milisecs

function initDrone(id, pos){

	console.log("new drone");	
	let droneGoal = [];
	// let goal = {position: new THREE.Vector3( 243,200,150 ), visited:false};
	// droneGoal.push(goal);
	let flowDeckSensor = {down:0};
	let multiRangerSensor = {front:Infinity, back:Infinity, up:Infinity,  left:Infinity, right:Infinity};
	let drone = {id:id, position:pos, flow: flowDeckSensor, multi:multiRangerSensor, goals: droneGoal};
	console.log(drone);

	return drone;
}

function setGoals(drone,goals){
	
	for(let i=0;i<goals.length;i++){
		let tmp = goals[i];
		let vec = new THREE.Vector3(tmp.x,tmp.y,tmp.z);
		let goal = {position: vec, visited:false};
		drone.goals.push(goal);
	}
	//drone.goals = goals;
	return drone;
}

//used with API that sets velocities
function setDroneVelocity(drone, velocity){
	let delta = deltaMovement*(deltaTime/100);

	if("zdistance" in velocity){
		let zd = velocity["zdistance"];
		// console.log(zd+"   "+drone.flow.down+"  "+(zd+zd-drone.flow.down));
		// let z = zd;
		// let diff = zd-drone.flow.down;
		// let z = drone.position.z+diff;
		// console.log(z);
		// if(drone.flow.down!=Infinity){
		// 	z = zd+(zd-drone.flow.down);
		// }
		drone.position.setZ(zd);
		// drone.position.setZ(zd);
	}
	if("vx" in velocity){
		let vel = velocity["vx"];
		vel = vel*delta
		let vx = vel+drone.position.x;
		drone.position.setX(vx);
		// let vy = -vel+drone.position.y;
		// drone.position.setY(vy);
	}
	if("vy" in velocity){
		let vel = velocity["vy"];
		vel = vel*delta
		let vy = vel+drone.position.y;
		drone.position.setY(vy);
		// let vy = -vel+drone.position.x;
		// drone.position.setX(vy);

	}
	if("yawrate" in velocity){
		//so far a noop
	}

	return drone;
}

function setDronePosition(drone,position){
	
	let goal = {position: drone.position.clone(), visited:false};
	if(position.x!=undefined){
		goal.position.setX(position.x);
	}
	if(position.y!=undefined){
		goal.position.setY(position.y);
	}
	if(position.z!=undefined){
		goal.position.setZ(position.z+(position.z-drone.flow.down));
	}
	
	let lastknownGoal = drone.goals[0];
	if(lastknownGoal!=undefined && lastknownGoal.position.distanceToSquared(goal.position)<25){
		//console.log("goal already in the one traveling to. Ignoring")
		//return drone;
	}else{
		drone.goals = [];
		drone.goals.push(goal);
		//drone.goals.unshift(goal);	
	}
	//console.log(drone.goals.length);
	
	return drone;

}
var first = 0;
//used with API that sets specific positions
function updateDrone(drone, structures, sensor){

	if(lastTime+deltaTime>Date.now()){
		return drone;
	}
	updateSensors(drone, structures, sensor);
	
	lastTime = Date.now()

	let goalIndex = _findNextGoalIndex(drone.goals);
	
	if(drone.goals.length==0){
		//no goals to goto
		return drone;
	}

	if(drone.goals.length>0 && Math.abs(drone.goals[goalIndex].position.lengthSq()-drone.position.lengthSq())<25){
		drone.goals[goalIndex].visited=true;
		return drone;
	}

	
	
	if(drone.goals.length>0){
		let goal = drone.goals[goalIndex];
		
		let vec = goal.position.clone().sub(drone.position);
		let dis = vec.clampLength(0,deltaMovement);
		

		drone.position = drone.position.add(dis);
	}

	//console.log("down: "+drone.flow.down+" up: "+drone.multi.up+" front: "+drone.multi.front+" back: "+drone.multi.back+" left: "+drone.multi.left+" right: "+drone.multi.right);

	console.log(drone.position);
	
	return drone;
}

function updateSensors(drone, structures, sensor){
	//check vicinity to structures
	//first we start with the height:
	sensor.set(drone.position,downVector);
	
	drone.flow.down = sensor.intersectObject(structures,true)[0]?sensor.intersectObject(structures,true)[0].distance:Infinity;
	//up
	sensor.set(drone.position, upVector);
	drone.multi.up = sensor.intersectObject(structures,true)[0]?sensor.intersectObject(structures,true)[0].distance:Infinity;
	//front
	sensor.set(drone.position,frontVector);
	drone.multi.front = sensor.intersectObject(structures,true)[0]?sensor.intersectObject(structures,true)[0].distance:Infinity;
	//back
	sensor.set(drone.position,backVector);
	drone.multi.back = sensor.intersectObject(structures,true)[0]?sensor.intersectObject(structures,true)[0].distance:Infinity;
	//left
	sensor.set(drone.position,leftVector);
	drone.multi.left = sensor.intersectObject(structures,true)[0]?sensor.intersectObject(structures,true)[0].distance:Infinity;
	//right
	sensor.set(drone.position,rightVector);
	drone.multi.right = sensor.intersectObject(structures,true)[0]?sensor.intersectObject(structures,true)[0].distance:Infinity;
	// console.log(drone.multi);
	return drone;
}

function _findNextGoalIndex(goals){
	for(let i=0; i<goals.length; i++){
		let goal = goals[i];
		if(!goal.visited){
			return i;
		}
	}
	//time for round robin
	// for(let i=0; i<goals.length; i++){
	// 	let goal = goals[i];
	// 	goal.visited = false;
	// }


	return 0;
}


export {initDrone,updateDrone, setDronePosition, setGoals,setDroneVelocity}


