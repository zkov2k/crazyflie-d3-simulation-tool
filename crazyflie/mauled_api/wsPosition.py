# -*- coding: utf-8 -*-
#
#     ||          ____  _ __
#  +------+      / __ )(_) /_______________ _____  ___
#  | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
#  +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
#   ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
#
#  Copyright (C) 2018 Bitcraze AB
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA  02110-1301, USA.
#from wsLog import LogConfig
#from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
import logging

log = log = logging.getLogger(__name__)


class Positioning:

    def __init__(self, crazyflie, rate_ms=100, zranger=False, is_sim=False):

        #Import and use different LogConfig, dependent on using a WS simulator        
        self.LogConfig = None
        if is_sim:
            log.info("starting with own LC")
            from mauled_api.wsLog import LogConfig
            self.LogConfig = LogConfig
        else:
            log.info("starting with CF LC")
            from cflib.crazyflie.log import LogConfig
            self.LogConfig = LogConfig

        


        if isinstance(crazyflie, SyncCrazyflie):
            self._cf = crazyflie.cf
        else:
            self._cf = crazyflie


        self._log_config = self._create_log_config(rate_ms)

        self._position = None
        

    def _create_log_config(self, rate_ms):

        lpos = self.LogConfig(name='Position', period_in_ms=rate_ms)
        lpos.add_variable('stateEstimate.x')
        lpos.add_variable('stateEstimate.y')
        lpos.add_variable('stateEstimate.z')

        lpos.data_received_cb.add_callback(self._data_received)

        return lpos

    def start(self):
        self._cf.log.add_config(self._log_config)
        self._log_config.start()

    def _convert_log_to_distance(self, data):
        if data is not None:
            return data/100
        return None

    def _data_received(self, timestamp, data, logconf):
        self._position = [
            data['stateEstimate.x'],
            data['stateEstimate.y'],
            data['stateEstimate.z']
        ]
        

    def stop(self):
        self._log_config.delete()


    # For call to str(). Prints readable form 
    def __str__(self): 
       return "X: "+str(self.x)+" Y: "+str(self.y)+" Z: "+str(self.z)

    @property
    def position(self):
        return self._position

    @property
    def x(self):
        return self._position[0]    

    @property
    def y(self):
        return self._position[1]    

    @property
    def z(self):
        return self._position[2]    

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()
