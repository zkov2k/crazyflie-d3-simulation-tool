import logging
import sys
import time
import json
import math
from datetime import datetime
import threading
import signal
from functools import partial

import cflib.crtp
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.positioning.position_hl_commander import PositionHlCommander
from cflib.positioning.motion_commander import MotionCommander
from src.motionController import MotionController
from src.sensorThread import SensorThread

#from sensorThread import SensorThread

from mauled_api.wsPosition import Positioning
import src.logHandler as logHandler
from src.ajustments import *

log = logHandler.configureLogger("testing", logging.DEBUG)

stopLock = threading.Lock()
eventLock = threading.Lock()
sleep = 0.0075


class FlyByDirection():
	def __init__(self, droneURI="http://localhost:5000",  is_sim=True):

		if is_sim:
			from mauled_api.wsCrazyflie import Crazyflie
			self._CF = Crazyflie
			from mauled_api.wsMultiranger import Multiranger
			self._MR = Multiranger
		else:
			from cflib.crazyflie import Crazyflie
			self._CF = Crazyflie
			from cflib.utils.multiranger import Multiranger
			self._MR = Multiranger

		self._is_sim = is_sim
		self._go_rigid = False


		self._droneURI=droneURI
		
		
		self._translate_x = 1.0
		self._translate_y = -1.0
		self._translate_z = 0

		self._eventHandling = False
		
		self._keep_flying = True
		self._addMc = None

		self._goals = self.loadGoals()
		self._currentGoal = None		



	def fly(self):

		
		_initial_position = [0,0,0]
		_initial_position = self._goals[0]["position"]
		# _initial_position[1]+=0.2
		counter = 0

		cflib.crtp.init_drivers(enable_debug_driver=False)
		cf = self._CF(rw_cache='./cache')
		with SyncCrazyflie(self._droneURI, cf=cf) as scf:

			log.info("Crazy init done")

			if hasattr(scf.cf.link, 'initDrone'):
				scf.cf.link.initDrone(176,_initial_position)

			
			log.info("Motionn init done")
			with self._MR(scf) as multiranger:
				log.info("MR init done")
				with Positioning(scf,is_sim=self._is_sim) as local:
					log.info("positioning init done")
					while local.position is None or multiranger.up is None:
						log.info("Waiting for sensor data..")
						time.sleep(0.5)
					# i=0
					# while i<100:
					# 	log.info(local.position)
					# 	i+=1
					# 	time.sleep(0.5)				
					# sys.exit(0)
					
					with MotionController(scf, local,default_height=0.50, default_velocity=0.5) as mc:
						log.info("mc init done")
						with eventLock:
							self._addMc = mc

						self._sensorThread = SensorThread(self.fromSensorThread, local,multiranger, mc)
						self._sensorThread.start()
						

						while True:
							if not self._keep_flying:
								break
							


							self._currentGoal = self._getNextGoal()
							log.info("next goal: "+str(self._currentGoal))

							reached = self._gotoPosition(self._currentGoal,local,mc, multiranger)

							if reached:
								#log.info("got goal")
								self._currentGoal["visited"] = True
							else:
								break
							





					log.warning("end motionController")
				log.warning("end positioning")
			log.warning("end MR")
		log.warning("end SCF")
			

		log.info("done!")


	def stop(self):
		log.info("calling stop on flying")
		with stopLock:
			self._keep_flying=False
		if self._sensorThread!=None:
			self._sensorThread.stop()
		with eventLock:
			self._addMc.stop()	
		print("all stopped")


	def _checkToFly(self):
		res = True

		stopLock.acquire()
		if not self._keep_flying:
			log.info("_keep_flying is False")
			res = False

		stopLock.release()
		return res


	def fromSensorThread(self,suspendNormal, directions=None,localization=None,mr=None):
		eventLock.acquire()
		self._eventHandling = suspendNormal
		eventLock.release()
		if self._eventHandling==True:
			log.info("We've got an issue from the sensors "+str(directions[1]))
			log.debug(directions)
			if directions[0]=="CATASTROPHE":
				#we can't handle this, so we're landing
				log.info("We have a CATASTROPHE! Landing....")
				log.info(directions)
				self.stop()

			eventLock.acquire()
			if not self._go_rigid:
				log.info("We're going RIGID!")
				self._go_rigid=True

			self._addMc.stop()
			time.sleep(2)
			res = True
			if "down" in directions[1]:
				log.debug(mr.down)
				#res =  self._handleHeightDifference(self._addMc, localization)

			elif "back" in directions[1]:
				self._addMc.right(0.1)
				# log.info("pos: "+str(localization.position))
				# log.info("sensors: "+str(mr))
				time.sleep(sleep*2)
				self._addMc.stop()
				res = False

			elif "front" in directions[1]:
				self._addMc.left(0.1)
				# log.info("pos: "+str(localization.position))
				# log.info("sensors: "+str(mr))
				time.sleep(sleep*2)
				self._addMc.stop()
				res = False

			elif "left" in directions[1]:
				self._addMc.front()
				time.sleep(sleep)
				self._addMc.stop()
				res = False

			elif "right" in directions[1]:
				self._addMc.back()
				time.sleep(sleep)
				self._addMc.stop()
				res = False
			elif "up" in directions[1]:
				self._addMc.down()
				time.sleep(sleep)
				self._addMc.stop()
				res=False

			eventLock.release()

			return res
		# else:
		# 	log.info("fly normal")
			# else:
			# 	print("GOGOGOGOGO")
				# log.warning(directions)
		# print("fST: "+str(eventLock.locked()))
			


	# def _handleHeightDifference(self, mc,localization):
	# 	log.info("Handling height issue")
	# 	tmpGoal = self._currentGoal["position"].copy()
		
	# 	dirX = 0 if tmpGoal[0]-localization.x==0 else ((tmpGoal[0]-localization.x)/abs(tmpGoal[0]-localization.x))*0.1
	# 	dirY = 0 if (tmpGoal[1]-localization.y)==0 else ((tmpGoal[1]-localization.y)/abs(tmpGoal[1]-localization.y))*0.1
	# 	# print(str(dirX)+"   _   "+str(dirY))
	# 	tmpGoal[0]=localization.x
	# 	tmpGoal[1]=localization.y
	# 	currentHeight = localization.z
	# 	diff = tmpGoal[2]-currentHeight
	# 	log.info("height diff: "+str(diff)+" current: "+str(currentHeight)+"  goal: "+str(tmpGoal[2]))

	# 	heightGoal = tmpGoal.copy()
	# 	while self._calcDistance(tmpGoal,localization.position)>0.05:
	# 		heightGoal[2]=localization.z+(diff/abs(diff)*0.01)
	# 		# log.info(localization.position)
	# 		mc.moveTowards(heightGoal,localization.position)	
	# 		log.info("height distance: "+str(self._calcDistance(tmpGoal,localization.position))+"     "+str(localization.position))
	# 		if not self._checkToFly():
	# 			return False
	# 		time.sleep(sleep)
		
	# 	log.info("reached wanted height")
	# 	mc.stop()

	# 	return True


	def _gotoPosition(self,goal,localization,motionController, multiranger):
		goalPos = goal["position"]
		#motionController.moveTowards(goalPos,localization.position)

		while True:
			if not self._checkToFly():
				return False
			eventLock.acquire()
			if self._eventHandling:
				# log.info("sleeping...waiting for event handling")
				eventLock.release()
				time.sleep(0.25)
				continue
			eventLock.release()

			eventLock.acquire()
			if self._go_rigid:
				# log.info(str(abs(localization.y-goalPos[1]))+"   "+str(localization.position)+"   "+str(goalPos))
				if abs(localization.y-goalPos[1])>0.05:
					#we go rigid
					goGoal=goalPos.copy()
					goGoal[0]=localization.x	
				else:
					goGoal=goalPos
			else:
				goGoal = goalPos
			# log.info(goGoal)
			eventLock.release()				
			# if abs(localization.y-goalPos[1])>0.05:
			# 	#we go rigid
			# 	goGoal=goalPos.copy()
			# 	goGoal[0]=localization.x
			# else:
			# 	goGoal = goalPos
			
			#debug info
			realGoal = goGoal.copy()
			realGoal[0] = realGoal[0]+self._translate_x
			realGoal[1] = realGoal[1]+self._translate_y
			
			# log.warning(localization.position)
			# log.warning("normal goal: "+str(goGoal)+" real: "+str(realGoal)+" dis: "+str(self._calcDistance(goGoal,localization.position))+"  loc: "+str(localization.position))
			with eventLock:
				# log.info("moving normally")
				motionController.moveTowards(goGoal,localization.position)
			time.sleep(sleep)
			
			if self._calcDistance(goGoal,localization.position)<0.05:
				log.debug(goGoal)
				#with eventLock:
				motionController.stop()
				time.sleep(1)
				if goGoal==goalPos:
					log.info("reached wanted position")
					return True



	# def _moveHorseShoe(self,coords,motionController,localization,multiranger):
	# 	i=0
	# 	for coord in coords:
	# 		log.info(str(i)+": moving in a horseshoe from:"+str(localization.position)+" to: "+str(coord))
	# 		i+=1
	# 		with eventLock:
	# 			motionController.moveTowards(coord,localization.position)
	# 		while self._calcDistance(coord,localization.position)>0.05:
	# 			time.sleep(sleep)
	# 			# print(self._calcDistance(coord,localization.position))
				

	
	def _getHorseShoe(self, direction, overAllGoal, local):
		deltaX = 0
		deltaY = 0
		returnDeltaX = 0
		if "back" in direction:
			deltaX+=0.25
			deltaY+=0.60
			returnDeltaX+=(deltaX+abs(overAllGoal[1]-local.position[1]))
		elif "front" in direction:
			deltaX-=0.25
			deltaY+=0.60
			returnDeltaX-=(deltaX+abs(overAllGoal[1]-local.position[1]))
		else:
			print(direction)

		#first leg
		first = local.position.copy()
		first[1]+=deltaX
		
		#second leg
		sec = first.copy()
		sec[0]+=deltaY

		#third leg
		third = sec.copy()
		#third[1]-=deltaX
		third[1]-=returnDeltaX

		return [first,sec,third]
			


	def _calcDistance(self, vec1, vec2):
		
		direction = [vec1[0]-vec2[0],vec1[1]-vec2[1],vec1[2]-vec2[2]]

		distance = math.sqrt(direction[0] * direction[0] +
                             direction[1] * direction[1] +
                             direction[2] * direction[2])
		return distance


	def loadGoals(self,uri="../webserver/static/goals.json"):

		file = open(uri)
		rawGoals = json.load(file)
		_goals = []

		for rawGoal in rawGoals["positions"]:
			
			goal = {}
			goal["position"] = []
			goal["position"].append(rawGoal[0]/100-self._translate_x)
			goal["position"].append(rawGoal[1]/100-self._translate_y)
			goal["position"].append(rawGoal[2]/100-self._translate_z)


			# goal["position"].append(rawGoal[1]/100-self._translate_y)
			# goal["position"].append(-(rawGoal[0]/100-self._translate_x))

			# goal["position"].append(rawGoal[2]/100-self._translate_z)
			#goal["position"]=rawGoal
			goal["visited"] = False
			goal["middle"] = False

			_goals.append(goal)
		return _goals

	def _getNextGoal(self):
	
		
		for goal in self._goals:
			if goal["visited"]==False:
				return goal	
		#round robin
		for goal in self._goals:
			goal["visited"]=False
		log.info("at last")
		return self._goals[0]










#def signal_handler(signum, **_):
def signal_handler(fbd, signum, x):
	log.info("Ctrl-c pressed!!")
	
	fbd.stop()
	sys.exit(0)



if __name__ == '__main__':
	log.info("start")
	
	
	#droneURI = "usb://0"
	droneURI = 'radio://0/80/2M'
	#fbd = FlyByDirection(droneURI,False)
	fbd = FlyByDirection()
	signal.signal(signal.SIGINT, partial(signal_handler, fbd))
	fbd.fly()
	#testing(False)
	print("af test")