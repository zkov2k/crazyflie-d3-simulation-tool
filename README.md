# Crazyflie D3.js Simulation Tool #

This is a Proof-of-Concept for simulating a Crazyflie 2.X with Flow deck v2 and Multiranger deck, in D3.js, where it is possible to build rooms and larger constructions. It is meant for a light-weight, easy simulation of control algorithms based on the above-mentioned sensors. 



### Quick description ###

This PoC has been created by mauling version 0.1.12.1 of the [Crazyflie python lib](https://github.com/bitcraze/crazyflie-lib-python "Crazyflie python lib"). 
It is written in Python 3.9.2, using Flask to serve the D3.js site and handle communication between the CFLib and the browser-based simulation. 
All communication between CFLib and the Flask server is handled via WebSockets.
To get this to work, certain files have had to be changed massively and these have been placed in the `mauled_api` folder.

It is necessary to change one file in the Crazyflie python lib to tell the system that a websocket driver is available. In `cflib/crtp/__init__py` two simple changes has to be made:
add: `from mauled_api.wsDriver import WebSocketDriver`
Change: `DRIVERS = [RadioDriver, SerialDriver, UdpDriver, DebugDriver, UsbDriver, PrrtDriver]` to `DRIVERS = [RadioDriver, SerialDriver, UdpDriver, DebugDriver, UsbDriver, PrrtDriver, WebSocketDriver]
Basically adding "WebSocketDriver" at the end of the list.

### Structure ###
#### Webserver ####
All files belonging to the Flask server, is found in the `webserver` folder. Any JS code central to this is found in `webserver/js`, similarly with static files, such as the building model and list of goals, which are found in `webserver/static

#### Crazyflie  ####
All files belonging to the Crazyflie code, is found in the `crazyflie` folder. In this folder, two example files are found as well. In `crazyflie/mauled_api` all the files from the API that needed to be changed are found. In `crazyflie/src` the files for simulation a CF in the browser are found.
The two examples are `testPositioning.py` which connects to a CF and tests the position estimates.
`flying.py` is the example that are used for simulation in D3. By changing a flag in this file, it will connect to a CF instead of the simulation.

### Setup ###

* run `$ pip install -r requirements.txt` to install all packages. I propose using a virtual environment for this.
* In `cflib/crtp/__init__py` change follwing:
add: `from mauled_api.wsDriver import WebSocketDriver`
Change: `DRIVERS = [RadioDriver, SerialDriver, UdpDriver, DebugDriver, UsbDriver, PrrtDriver]` to `DRIVERS = [RadioDriver, SerialDriver, UdpDriver, DebugDriver, UsbDriver, PrrtDriver, WebSocketDriver]`
* in `flying.py` set droneURI to correct URI for CF and initialize FlyByDirection with that URI and a flag `is_sim=False` to connect to a CF. For simulating, just initialize FlyByDirection without any parameters.

### Run ###

* run `cd webserver; python app.py`
* Open new terminal, and run: `cd crazyflie; python flying.py`

