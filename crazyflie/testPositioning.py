"""
This is used to check the movement of the CF
"""

import logging
import sys
import time
import json
import math
from datetime import datetime
import threading
import signal
from functools import partial

import cflib.crtp
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.positioning.position_hl_commander import PositionHlCommander
from cflib.positioning.motion_commander import MotionCommander
#from crazyflie.mauled_api.motionController import MotionController
from src.motionController import MotionController


from mauled_api.wsPosition import Positioning
import src.logHandler as logHandler
from src.ajustments import *

log = logHandler.configureLogger("testing", logging.INFO)



class FlyByDirection():
    def __init__(self, droneURI="http://localhost:5000",  is_sim=True):

        if is_sim:
            from wsCrazyflie import Crazyflie
            self._CF = Crazyflie
            from wsMultiranger import Multiranger
            self._MR = Multiranger
        else:
            from cflib.crazyflie import Crazyflie
            self._CF = Crazyflie
            from cflib.utils.multiranger import Multiranger
            self._MR = Multiranger

        self._is_sim = is_sim


        self._droneURI=droneURI
        print(self._droneURI)

        
        
        self._keep_flying = True
        self._addMc = None
               



    def fly(self):

        #_initial_position = [1,1,0]
        #_initial_position = self._getNextGoal()["position"].copy()
        #_initial_position[2]=0
        #_initial_position=[1.67,1.44,0.3]
        _initial_position = [0,0,0]
        counter = 0

        cflib.crtp.init_drivers(enable_debug_driver=False)
        cf = self._CF(rw_cache='./cache')
        with SyncCrazyflie(self._droneURI, cf=cf) as scf:

            log.info("Crazy init done")

            if hasattr(scf.cf.link, 'initDrone'):
                scf.cf.link.initDrone(176,_initial_position)

            
            log.info("Motionn init done")
            with self._MR(scf) as multiranger:
                log.info("MR init done")
                with Positioning(scf, is_sim=self._is_sim) as local:
                    log.info("positioning init done")
                    while local.position is None or multiranger.up is None:
                        log.info("Waiting for sensor data..")
                        time.sleep(0.5)                     
                    
                    with MotionController(scf, local,default_height=0.5, default_velocity=0.2) as mc:
                        log.info("mc init done")


                        self._addMc = mc
                        while True:
                            if not self._keep_flying:
                                break
                            print(str(local.position)+"start: ")
                            time.sleep(5)
                            mc.back()
                            time.sleep(2)
                            mc.stop()
                            time.sleep(1)
                            print(str(local.position)+"after back: ")
                            mc.right()
                            time.sleep(2)
                            mc.stop()
                            time.sleep(1)
                            print(str(local.position)+"after right: ")
                            mc.front()
                            time.sleep(2)
                            mc.stop()
                            time.sleep(1)
                            print(str(local.position)+"after front: ")
                            mc.left()
                            time.sleep(2)
                            mc.stop()
                            time.sleep(1)
                            print(str(local.position)+"after left: ")
                            mc.land()
                            print(str(local.position)+"after land: ")
                            self._keep_flying = False
                            





                    log.warning("end motionController")
                log.warning("end positioning")
            log.warning("end MR")
        log.warning("end SCF")
            

        print("done!")


    def stop(self):
        print("calling stop on flying")
        self._keep_flying=False




#def signal_handler(signum, **_):
def signal_handler(fbd, signum, x):
    log.info("Ctrl-c pressed!!")
    print(fbd)
    # print(x)
    # message = f'Signal handler called with signal {signum}'
    # log.info(message)
    # # do things
    # #self.sio.disconnect()
    fbd.stop()
    sys.exit(0)



if __name__ == '__main__':
    log.info("start")
    
    
    #droneURI = "usb://0"
    droneURI = 'radio://0/80/2M'
    fbd = FlyByDirection(droneURI,False)
    signal.signal(signal.SIGINT, partial(signal_handler, fbd))
    fbd.fly()
    #testing(False)
    print("af test")