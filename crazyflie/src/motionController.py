import logging
import math
import sys
import time

from cflib.positioning.motion_commander import MotionCommander


import src.logHandler as logHandler

log = logHandler.configureLogger("motionController", logging.WARNING)


class MotionController():

	def __init__(self, scf, localization, default_height=0.3, default_velocity=0.2):
		log.info("init MC")
		self._mc = MotionCommander(scf, default_height)
		self._default_height = default_height
		
		self.default_velocity = default_velocity
		self._localization = localization
		self._velVector = [0,0,0]





	def goto(self,newPos,currentPos, velocity=None):
		self.moveTowards(newPos,currentPos,velocity)
		while self._calcDistance(newPos,self._localization.position)>0.05:
			time.sleep(0.0025)

		return True




	def moveTowards(self, newPos, currentPos, velocity=None):
		if newPos==currentPos:
			return

		if velocity==None:
			velocity=self.default_velocity
		
		velocityVec = self._calcVelocity(newPos,currentPos,velocity)
		# log.warning(str(currentPos)+"     "+str(newPos)+"   "+str(velocityVec)+"    "+str(self._velVector))
		if velocityVec==self._velVector:
			return
		else:
			log.debug("new vector..sending")
		self._velVector = velocityVec
		
		self._mc.start_linear_motion(velocityVec[0],velocityVec[1],velocityVec[2])



		
	def height(self, distance_m):
		self._mc.up(distance_m)

	def left(self,speed=None):
		currentPos = self._localization.position
		newPos = [currentPos[0],currentPos[1]+1,currentPos[2]]
		self._velVector = [0,0,0]
		self.moveTowards(newPos,currentPos,speed)


	def right(self,speed=None):
		currentPos = self._localization.position
		newPos = [currentPos[0],currentPos[1]-1,currentPos[2]]
		self._velVector = [0,0,0]
		self.moveTowards(newPos,currentPos,speed)

	def front(self,speed=None):
		currentPos = self._localization.position
		newPos = [currentPos[0]+1,currentPos[1],currentPos[2]]
		self._velVector = [0,0,0]
		self.moveTowards(newPos,currentPos,speed)

	def back(self,speed=None):
		currentPos = self._localization.position
		newPos = [currentPos[0]-1,currentPos[1],currentPos[2]]
		self._velVector = [0,0,0]
		self.moveTowards(newPos,currentPos,speed)

	def down(self,speed=None):
		currentPos = self._localization.position
		newPos = [currentPos[0],currentPos[1],currentPos[2]-1]
		self._velVector = [0,0,0]
		self.moveTowards(newPos,currentPos,speed)

	def stop(self):
		self._mc.stop()


	def land(self):
		self._mc.land()

	def __enter__(self):
		self._mc.take_off()
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		log.info("exiting motionController")
		self._mc.land()



	def _calcVelocity(self, newPos, currentPos, velocity):
		
		distance = self._calcDistance(newPos,currentPos)

		distance_vec = self._subVector(newPos,currentPos)

		# print(distance)
		# print(velocity)
		flight_time = distance / velocity

		velocity_x = velocity * distance_vec[0] / distance
		velocity_y = velocity * distance_vec[1] / distance
		velocity_z = velocity * distance_vec[2] / distance
		return [velocity_x,velocity_y,velocity_z]



	def _calcDistance(self, newPos, currentPos):
		
		direction = self._subVector(newPos, currentPos)

		distance = math.sqrt(direction[0] * direction[0] +
                             direction[1] * direction[1] +
                             direction[2] * direction[2])
		return distance

	def _subVector(self, vec1, vec2):
		
		res = [vec1[0]-vec2[0],vec1[1]-vec2[1],vec1[2]-vec2[2]]
		return res


