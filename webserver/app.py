from flask import Flask, render_template,send_from_directory, json
from flask_socketio import SocketIO, emit
import mimetypes
import os
import json
import logging
import sys

import traceback 


# Log file location
logfile = 'logs/webserver.log'
# Define the log format
log_format = (
    '[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s')


# Define basic configuration
logging.basicConfig(
    # Define logging level
    level=logging.INFO,
    # Declare the object we created to format the log messages
    format=log_format,
    # Declare handlers
    handlers=[
        logging.FileHandler(logfile),
        logging.StreamHandler(sys.stdout),
    ]
)
log = logging.getLogger("WebServer")

mimetypes.add_type('application/javascript', '.js')
app = Flask(__name__, static_url_path='',static_folder='templates')
#socketio = SocketIO(app, logger=True, engineio_logger=True)
socketio = SocketIO(app)
#socketio = SocketIO(app, logger=True)

@app.route('/')
def index():
	return app.send_static_file('index.html')

@app.route('/js/<path:path>')
def send_js(path):
	ret = send_from_directory('js', path)
	return ret

@app.route('/objects/<path:path>')
def send_obj(path):
	ret = send_from_directory('static', path)
	return ret

@app.route('/conf/<path:path>')
def send_conf(path):
	#ret = send_from_directory('templates/conf', path)
	SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
	json_url = os.path.join(SITE_ROOT, 'static', path)
	f = open(json_url)
	#print(f.read())
	data = json.loads(f.read())
	#print(ret)
	#return json.dumps(ret)
	return data



@socketio.on('connect')
def test_connect():
	log.info("Here we goooooo!")
	emit('after connect', {'data': 'Connected'})
	emit("drone_response",[0,4],broadcast=True)

@socketio.on('disconnect')
def test_disconnect():
    log.info('Client disconnected!!')

@socketio.on('new_drone')
def new_drone(data):
	log.info("initPos: "+str(data))
	data[1][0]*=100
	data[1][1]*=100
	data[1][2]*=100
	emit("init_drone",data,broadcast=True)
	#return 'one', 2


@socketio.on('from_drone')
def from_drone(drone):
	
	json = _convertCf2Viz(drone)

	# log.info("FROM DRONE")
	# log.info(drone)
	# log.info("TO VIZ")
	# log.info(json)
	
	# print(json)
	# print("after drone")
	emit("to_viz",json,broadcast=True)
	return 'one', 2


@socketio.on('from_viz')
def drone_viz(drone):
	
    #print(sid)
    #NOTE: convert distance to meters
	
	#print("FROM VIZ")
	
	cf = _convertViz2Cf(json.loads(drone))
	log.info(cf["stateEstimate.z"])

	emit("to_drone",cf,broadcast=True)

def _convertCf2Viz(str):
	drone = None
	try:
		drone = json.loads(str)
		if "position" in drone:
			pos = drone['position']
			if "x" in pos:
				drone['position']['x']*=100
			if "y" in pos:
				drone['position']['y']*=100
			if "z" in pos:
				drone['position']['z']*=100

		elif "velocity" in drone:

			vel = drone["velocity"]
			if "vx" in vel:
				drone["velocity"]["vx"]*=100
			if "vy" in vel:
				drone["velocity"]["vy"]*=100
			if "yawrate" in vel:
				drone["velocity"]["yawrate"]*=100
			if "zdistance" in vel:
				drone["velocity"]["zdistance"]*=100
		#return drone;
	except:
		print(drone)
	
	return drone

	
	

def _convertViz2Cf(json):
	#print(json["position"])
	#print(json)
	# print(type(json))
	# print("\n")
	drone = {}
	drone["range.front"] = 9999 if json["multi"]["front"]==None else json["multi"]["front"]*10;
	drone["range.back"] = 9999 if json["multi"]["back"]==None else json["multi"]["back"]*10;
	drone["range.left"] = 9999 if json["multi"]["left"]==None else json["multi"]["left"]*10;
	drone["range.right"] = 9999 if json["multi"]["right"]==None else json["multi"]["right"]*10;
	drone["range.up"] = 9999 if json["multi"]["up"]==None else json["multi"]["up"]*10;
	drone["range.zrange"] = 9999 if json["flow"]["down"]==None else json["flow"]["down"]*10;
	drone["counter"] = json["counter"];

	drone["stateEstimate.x"] = json["position"]["x"]/100;
	drone["stateEstimate.y"] = json["position"]["y"]/100;
	#drone["stateEstimate.z"] = json["position"]["z"]/100;
	drone["stateEstimate.z"] = 0 if json["flow"]["down"]==None else json["flow"]["down"]/100;

	return drone

if __name__ == '__main__':
	#socketio.run(app)
	log.info("Starting webserver")
	socketio.run(app, debug=True)
