

import * as THREE from './three/build/three.module.js';

import { OrbitControls } from './three/examples/jsm/controls/OrbitControls.js';
import Stats from './three/examples/jsm/libs/stats.module.js';
import { STLLoader } from './three/examples/jsm/loaders/STLLoader.js';
//import { GUI } from './three/examples/jsm/libs/dat.gui.module.js';

//import { DragControls } from './three/examples/jsm/controls/DragControls.js';
//import { TransformControls } from './three/examples/jsm/controls/TransformControls.js'

//import * as UnitController from "./controller.js";

import {sendMessage}  from "./controller.js"
import * as Drone from "./drone.js"

/*String.prototype.format = function () {

	var str = this;

	for ( var i = 0; i < arguments.length; i ++ ) {

		str = str.replace( '{' + i + '}', arguments[ i ] );

	}
	return str;

};*/

var stats;
var camera, scene, renderer, wallsGroup, goalsGroup, structuresGroup;

var drones = {};
var lastSent = Date.now();
var deltaSendToWs = 250 //milli
var goals = [];
var stopAnimation = false;


var container = document.getElementById( 'container' );

var raycaster = new THREE.Raycaster();
var intersects;
var mouse = new THREE.Vector3();
var width = 3000;
var height = 3000;

var translateX =0;
var translateY = 9;

var controls; //camera controls



container.addEventListener('mousemove', onDocumentMouseMove, false);
container.addEventListener('mousedown', onDocumentMouseDown, false);
document.addEventListener('keyup', onDocumentKeyUp, false);


window.addEventListener('load', function () {
	init();
	animate();
})


function init() {

	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xf0f0f0 );

	camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.up.set(0,0,1);
	camera.position.set( -500, 0, 1000 );
	// camera.position.set( -106, -121, 652 );
	
	scene.add( camera );

	

	scene.add( new THREE.AmbientLight( 0xf0f0f0 ) );
	var light = new THREE.SpotLight( 0xffffff, 1.5 );
	light.position.set( 100, 1500, 200 );
	light.angle = Math.PI * 0.2;
	light.castShadow = true;
	light.shadow.camera.near = 200;
	light.shadow.camera.far = 2000;
	light.shadow.bias = - 0.000222;
	light.shadow.mapSize.width = 1024;
	light.shadow.mapSize.height = 1024;
	scene.add( light );

	var planeGeometry = new THREE.PlaneBufferGeometry( width, height );
	//planeGeometry.rotateX( - Math.PI / 2 );
	
	//var planeMaterial = new THREE.ShadowMaterial( {  opacity: 0.2 } );
	let planeMaterial = new THREE.MeshStandardMaterial({ color: 0xffaa00, transparent: true, opacity:0.2 })

	var plane = new THREE.Mesh( planeGeometry, planeMaterial );
	//plane.position.y = - 200;
	plane.receiveShadow = true;
	plane.name = "plane";
	//scene.add( plane );
	planeGeometry.computeBoundingBox();

	
	var helper = new THREE.GridHelper( width, 100, 0xff00ff  );
	helper.geometry.rotateX( Math.PI / 2 );
	//helper.position.y = - 20;
	helper.material.opacity = 0.25;
	helper.material.transparent = true;
	scene.add( helper );

	wallsGroup = new THREE.Group();
	wallsGroup.name="walls";
	// wallsGroup.rotateZ( - Math.PI / 2 );
	//scene.add(wallsGroup);

	goalsGroup = new THREE.Group();
	goalsGroup.name="goals";
	// goalsGroup.rotateZ( - Math.PI / 2 );
	//goals.rotateX( - Math.PI / 2 );
	scene.add(goalsGroup);
	structuresGroup = new THREE.Group();
	structuresGroup.name="Structures";

	structuresGroup.add(wallsGroup);
	structuresGroup.add(plane);
	scene.add(structuresGroup);

	//var axes = new AxesHelper( 1000 );
	//axes.position.set( - 500, - 500, - 500 );
	//scene.add( axes );

	renderer = new THREE.WebGLRenderer( { antialias: true } );

	
	renderer.setPixelRatio( window.devicePixelRatio );
	//renderer.setSize( window.innerWidth, window.innerHeight );
	
	renderer.setSize( container.clientWidth, container.clientHeight );
	renderer.shadowMap.enabled = true;
	container.appendChild( renderer.domElement );

	stats = new Stats();
	container.appendChild( stats.dom );

	// Controls
	controls = new OrbitControls( camera, renderer.domElement );
	controls.damping = 0.2;
	controls.addEventListener( 'change', render );


	/*var gui = new GUI();

	gui.add( params, 'uniform' );
	gui.add( params, 'tension', 0, 1 ).step( 0.01 ).onChange( function ( value ) {
		splines.uniform.tension = value;
		updateSplineOutline();
	} );

	gui.add( params, 'centripetal' );
	gui.add( params, 'chordal' );
	gui.add( params, 'addPoint' );
	gui.add( params, 'removePoint' );
	gui.add( params, 'exportSpline' );
	gui.open();*/


	loadBuilding();
	loadGoals();
	
}

function loadBuilding(){
	console.log("loading building")
	$.getJSON("conf/building.json", function(data){
		// $.getJSON("conf/appartment.json", function(data){
           
            let tmpTransX = data["translateX"];
            if(tmpTransX!==undefined){
            	translateX = tmpTransX;
            }
            let tmpTransY = data["translateY"];
            if(tmpTransY!==undefined){
            	translateY = tmpTransY;
            }

			showBuilding(data);
        }).fail(function(err){
            console.log("not able to load building");
            console.log(err);
        });
}

function showBuilding(json){
	
	let wallHeight = json["height"]
	let ww = 10;
	
	let externalWalls = json["outer-walls"];
	if( externalWalls !=undefined){
		for (let i = 1; i < externalWalls.length; i++) {
			// let shape = new THREE.Shape();
			// shape.moveTo(externalWalls[i-1]["x"],externalWalls[i-1]["y"]);
			let wallGeometry = _buildWallGeo(externalWalls[i-1]["x"],externalWalls[i-1]["y"],externalWalls[i]["x"],externalWalls[i]["y"],ww,wallHeight);

			let material = new THREE.MeshStandardMaterial({ color: 0x311206, transparent: true, opacity:0.2 })
			let wall = new THREE.Mesh(wallGeometry, material);
			wall.receiveShadow = true;
			//wall.translateY(height);
			wall.name="outer-wall-"+i;
			//wall.rotateX(Math.PI / 2);
			wallsGroup.add(wall);
		}
	}

	let internalWalls = json["inner-walls"];
	
	if(internalWalls !=undefined){


		for (let i = 0; i < internalWalls.length; i++) {
			
			let wallGeometry = _buildWallGeo(internalWalls[i]["start"]["x"],internalWalls[i]["start"]["y"],internalWalls[i]["end"]["x"],internalWalls[i]["end"]["y"],ww,wallHeight);

			
			let material = new THREE.MeshStandardMaterial({ color: 0xe2e2e2, transparent:true, opacity: 0.5 })
			let wall = new THREE.Mesh(wallGeometry, material);
			wall.name="inner-wall-"+i;
			wall.receiveShadow = true;
			
			wallsGroup.add(wall);
		}
	}

	let interior = json["interior"]
	if(interior != undefined){
		for(let i=0; i<interior.length; i++){
			let obj = interior[i];
			let interiorGeometry = new THREE.BoxGeometry(obj["deltaX"], obj["deltaY"], obj["height"], 10, 10, 10);
			let material = new THREE.MeshNormalMaterial({ side: THREE.DoubleSide });
			let box = new THREE.Mesh(interiorGeometry, material);
			let x = obj["x"]+(obj["deltaX"]/2);
			let y = obj["y"]+(obj["deltaY"]/2);
			let z = obj["z"]+(obj["height"]/2);
			
			box.position.set( x,y, z);
			box.name = "interior-"+i;
			box.receiveShadow=true;

			wallsGroup.add(box);
		}
	}


	_centerBuilding(wallsGroup);
	//add a transparent ceilling to utilize raycaster
	var ceilGeo = new THREE.PlaneBufferGeometry( width, height );
	ceilGeo.computeBoundingBox();
	
	let ceilMaterial = new THREE.MeshStandardMaterial({ color: 0xffa0cc, transparent: true, opacity:0.3 })

	var ceilling = new THREE.Mesh( ceilGeo, ceilMaterial );
	ceilling.name = "ceilling";
	
	ceilling.translateZ(wallHeight);
	ceilling.rotateX(Math.PI );
	structuresGroup.add(ceilling);
}

function _centerBuilding(object3d){
	let bb = new THREE.Box3().setFromObject(object3d);
	let center = bb.getCenter(new THREE.Vector3());

	
	object3d.translateX(translateX);
	object3d.translateY(translateY);

	goalsGroup.translateX(translateX);
	goalsGroup.translateY(translateY);
	// object3d.translateX(-60);
	// object3d.translateY(-188);

	// goalsGroup.translateX(-60);
	// goalsGroup.translateY(-188);

}


function _buildWallGeo(startX,startY,endX,endY,width,height){
	let shape = new THREE.Shape();
	//console.log(startX,startY,endX,endY);
	shape.moveTo(startX,startY);
	
	shape.lineTo(startX+width,startY);
	shape.lineTo(startX+width,startY+width);
	shape.lineTo(endX+width,endY+width);
	shape.lineTo(endX+width,endY);
	shape.lineTo(endX,endY);
	shape.lineTo(startX,startY);

	let wallGeometry = new THREE.ExtrudeBufferGeometry(shape , {
	    steps: 10,
	    depth: height,
	    bevelEnabled: false,
	    curveSegments: 32
	});

	return wallGeometry;
}


function loadGoals(){
	console.log("loading goals")
	$.getJSON("conf/goals.json", function(data){
            
            showGoals(data);
        }).fail(function(err){
            console.log("not able to load goals");
            console.log(err);
        });
}

function showGoals(goalList){
	
	//goals = goalList["positions"];
	for(let i=0; i<goalList["positions"].length; i++){
		//let pos = goals[i];
		let goal = goalList["positions"][i];
		let pos = new THREE.Vector3(goal[0],goal[1],goal[2]);

		goals.push(pos);

		var cone = new THREE.ConeGeometry(10, 40),
		material = new THREE.MeshStandardMaterial({
			color: 0x00ff00
		});

		let mesh = new THREE.Mesh(cone, material);
		mesh.name="goals-"+i;
		mesh.position.set(pos.x,pos.y,pos.z);
		mesh.rotateX(Math.PI / 2 );
		goalsGroup.add(mesh);
	}
	//update drones goals
	//console.log("call initDrone")
	//initDrone(123,[100,50,100]);
}

function initDrone(id,position){
	console.log(position);
	if (id in drones){
		console.log("drone already in scene. Resetting start position");
		let drone = drones[id];
		drone.data.position = new THREE.Vector3(position[0],position[1],position[2]);
		drone.mesh.position.set(position[0],position[1],position[2])
		drone.pathTraveled = [drone.mesh.position.clone()];

		return;
	}

	const loader = new STLLoader();
	const loadDrone = new Promise(function(resolve, reject){
		loader.load( 'objects/drone.stl', function ( geometry ) {
			resolve(geometry)

		});
	}).then(geometry =>{
		console.log("done loading drone");
		const material = new THREE.MeshPhongMaterial( { color: 0xff5533, specular: 0x111111, shininess: 200 } );
		const mesh = new THREE.Mesh( geometry, material );

		
		//mesh.rotation.set( 0, - Math.PI / 2, 0 );
		mesh.scale.set( 0.025, 0.025, 0.025 );
		mesh.name = "drone-"+id;
		mesh.castShadow = true;
		mesh.receiveShadow = true;
		mesh.position.set( 0, 0, 0);

		let pos = new THREE.Vector3();
		if(position!=undefined){
			pos = new THREE.Vector3(position[0],position[1],position[2]);
			mesh.position.set(position[0],position[1],position[2]);
		}
		let drone = {mesh:mesh, data: Drone.initDrone(id, pos), sensor: new THREE.Raycaster(), pathTraveled:[mesh.position.clone()] };
		//Drone.setGoals(drone.data,goals);
		console.log(drone);
		drones[id] = drone;		

		scene.add( mesh );
		drone.mesh.add(_buildAxes(2000));

		addUIElement(drone);
	});
	
}



function updateDrone(droneData){
	
	//debugging
	if(Object.keys(drones).length==0){
		initDrone(176,[0,0,0]);
	}
	// let vx = droneData["velocity"]["vx"];
	// let vy = droneData["velocity"]["vy"];
	// let vz = droneData["velocity"]["zdistance"];
	// let dst = new THREE.Vector3( vx, vy, vz);
	// dst = dst.add(drone.data.position);

	Object.keys(drones).forEach((key, index) => {
		drone = drones[key];

		//scene.add(_buildAxis(drone.data.position,dst,0xff00ff));
		if("position" in droneData){

			drone.data = Drone.setDronePosition(drone.data,droneData["position"]);	
		}else if("velocity" in droneData){
			drone.data = Drone.setDroneVelocity(drone.data,droneData["velocity"]);	
		}
		
		
	});
}

function _buildAxes( length ) {
    var axes = new THREE.Object3D();

    axes.add( _buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( length, 0, 0 ), 0xFF0000, false ) ); // +X
    axes.add( _buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( -length, 0, 0 ), 0xFF0000, true) ); // -X
    axes.add( _buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, length, 0 ), 0x00FF00, false ) ); // +Y
    axes.add( _buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, -length, 0 ), 0x00FF00, true ) ); // -Y
    axes.add( _buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, length ), 0x0000FF, false ) ); // +Z
    axes.add( _buildAxis( new THREE.Vector3( 0, 0, 0 ), new THREE.Vector3( 0, 0, -length ), 0x0000FF, true ) ); // -Z

    return axes;

}

function _buildAxis( src, dst, colorHex, dashed ) {
    var geom = new THREE.Geometry(),
        mat; 

    if(dashed) {
        mat = new THREE.LineDashedMaterial({ linewidth: 3, color: colorHex, dashSize: 3, gapSize: 3 });
    } else {
        mat = new THREE.LineBasicMaterial({ linewidth: 3, color: colorHex });
    }

    geom.vertices.push( src.clone() );
    geom.vertices.push( dst.clone() );
    //geom.computeLineDistances(); // This one is SUPER important, otherwise dashed lines will appear as simple plain lines

    var axis = new THREE.Line( geom, mat, THREE.LineSegments );

    return axis;

}


function animate() {

	requestAnimationFrame( animate );
	render();
	stats.update();

}

var drone;
var first = true;
function render() {

	if(!stopAnimation){
		Object.keys(drones).forEach((key, index) => {
			drone = drones[key];
			drone.data = Drone.updateDrone(drone.data,structuresGroup, drone.sensor);
			drone.mesh.position.set(drone.data.position.x,drone.data.position.y,drone.data.position.z);
			updateUIFields(drone);
			if(drone.pathTraveled.slice(-1)[0].distanceToSquared(drone.mesh.position)>10){
				drone.pathTraveled.push(drone.mesh.position.clone());
			}	
			
		});	

		if(lastSent+deltaSendToWs<=Date.now()){
			Object.keys(drones).forEach((key, index) => {
				sendMessage(drones[key]);
			});
			lastSent = Date.now();
		}
	}
	

	renderer.render( scene, camera );
}







function addUIElement(agent){
	let tmp = document.getElementsByClassName("agentHolder")[0];
	
	let ui = tmp.cloneNode(true);
	ui.style.display = "inherit";
	
	ui.id = "ah-"+agent.data.id;
	
	document.getElementById("gui").appendChild(ui);
	updateUIFields(agent);

	
	let show=false;
	ui.querySelector("#missionPathBtn").addEventListener('mousedown', function(evt){
		
		goalsGroup.visible = show;
		show=!show;
	}, false);

	ui.querySelector("#traveledPathBtn").addEventListener('mousedown', function(evt){
		let id = "path-"+agent.data.id;
		let poi = scene.getObjectByName(id);
		
		if(poi==undefined){
			let geometry = new THREE.BufferGeometry().setFromPoints(agent.pathTraveled);
			let material = new THREE.LineBasicMaterial( { color: 0x00 } );

			let line = new THREE.Line( geometry, material );
			line.name = id;
			scene.add( line );

		}else{
			scene.remove(poi);
		}

	}, false);

	
}

function updateUIFields(agent){
	let id = "#ah-"+agent.data.id;
	let ui = document.querySelector(id);
	//set info
	ui.getElementsByClassName("title")[0].innerHTML = agent.data.id;
	//ui.getElementsByClassName("vContent")[0].innerHTML = ""+agent.data.vizPos.x+",<br/>"+agent.data.vizPos.y+",<br/>"+agent.data.vizPos.z+"";
	ui.getElementsByClassName("rContent")[0].innerHTML = ""+Math.round(agent.data.position.x)+",<br/>"+Math.round(agent.data.position.y)+",<br/>"+Math.round(agent.data.position.z)+"";
}



function onDocumentKeyUp(event){
	if(event.key=="r"){
		console.log("reset cam");
		if(controls!=undefined){
			controls.reset();
		}
		
	}else if(event.key=="s"){
		stopAnimation=!stopAnimation;
	}
}



function onDocumentMouseMove(event) {
	event.preventDefault();

	/* mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = -(event.clientY / window.innerHeight) * 2 + 1; */
	mouse.x = (event.clientX / document.getElementById("container").offsetWidth) * 2 - 1;
	mouse.y = -(event.clientY / document.getElementById("container").offsetHeight) * 2 + 1;
	
}





function onDocumentMouseDown(event) {
	event.preventDefault();
  	console.info('mouseDown');
  	console.log("camera pos:")
  	// console.log(camera.position)
  	var vector = new THREE.Vector3( mouse.x, mouse.y, -1 ).unproject( camera );

	//let poi = scene.getObjectByName("plane");

	//console.log(poi);
	raycaster.setFromCamera( mouse, camera );
	intersects = raycaster.intersectObjects(scene.children, true);
	if(intersects.length>0){
		console.log(intersects[0].object.name);
		//console.log(intersects[0].object.position);
		console.log("viz pos:")
		console.log(intersects[0].point);
		console.log("real pos:");
		let tmpPos = intersects[0].point;
		tmpPos.x-=translateX;
		tmpPos.y-=translateY;
		console.log(tmpPos);
	}else{
		console.log("no intersects");
	}
	/*var vec = new THREE.Vector3(); // create once and reuse
	var pos = new THREE.Vector3(); // create once and reuse

	vec.set(
	   mouse.x, mouse.y * 2 + 1, 0.5 );

	vec.unproject( camera );

	vec.sub( camera.position ).normalize();

	var distance = - camera.position.z / vec.z;

	pos.copy( camera.position ).add( vec.multiplyScalar( distance ) );
	console.log(pos);*/
}

export {initDrone,updateDrone}